# Dice Castle

## Lancer les tests

```
./composer install

```

puis :

```
phpunit --bootstrap vendor/autoload.php tests --filter testNotFailing

```
ou

```
phpunit test

```

## Création d'une branche 

```
git checkout -b nom.prenom
```

## Commits

Ajoutez des commits aussi souvent que nécessaire, pour me montrer le code intermédiaire.

Le message du commit doit être clair (pour vous et pour les personnes qui vont le relire).

Lorsque vous faites un commit, les tests sont aux verts. 

(Pensez au cycle du TDD avec commit : Red -> Green -> Refactor -> Commit)

## Push

```
git push origin nom-prenom
```

# Dice Castle

Un héros entre dans un chateau à la recherche de l'épée magique.
Il va devoir affronter des monstres et des pièges.

## Fonctionnalités

### Itération 1 :
 * Le héros démarre la partie à l'extérieur du chateau.
 * A chaque tour, le héros avance d'une case.
  * Le chateau est constitué d'un enchainement de 17 cases.
 * Les cases de 1 à 16 sont vides.
 * A la fin de son déplacement, le joueur sait sur quelle case se trouve le héros.
 * la case 17 contient l'épée magique, si le héros arrive sur cette case, il obtient l'épée.
 * La partie se termine lorsque le héros obtient l'épée magique.

### Itération 2 : 
 * Le héros peut se déplacer dans le chateau en lançant un d6.
 * Lorsqu'il arrive ou dépasse la case 17, il obtient l'épée magique.

### Itération 3 :
 * Le héros commence l'aventure avec 3 points de vie.
 * Le héros a un maximum de 5 points de vie.
 * Le héros peut perdre des points de vie lorsqu'il perd un combat contre un monstre.
 * Si le héros à moins de 1 point de vie, il est mort, la partie est terminée.

### Itération 4 :
 * Les cases '2' et '5' contiennent des monstres de niveau 1.
 * La case '15' contient un monstre de niveau 2.
 * La case '16' contient un monstre de niveau 3 (le Roi Squelette).
 * La case "13' contient 2 potions de soin.
 * Une potion de soin permet d'ajouter un point de vie.

#### Résolution des cases monstres :
 * un monstre de niveau 1 => lancer un d6 :
    * si le résultat est 1 ou 2 : le héros perd 1 point de vie.
    * si le résultat est 3, 4, 5, 6 : le héros ne perd pas de point de vie.
 * un monstre de niveau 2 => lancer un d6 :
    * si le résultat est 1, 2, 3 : le héros perd 1 point de vie.
    * si le résultat est 4, 5, 6 : le héros ne perd pas de point de vie.
  * un monstre de niveau 3 => lancer un d6 :
      * si le résultat est 1, 2, 3 : le héros perd 2 points de vie.
      * si le résultat est 4, 5, 6 : le héros ne perd pas de point de vie.

### Itération 5 : 
 * Le héros commence l'aventure avec 0 pièces d'or.
 * Le héros possède une bourse pouvant contenir un maximum de 20 pièces d'or.
 * Vous récupérez maintenant des pièces d'or en tuant les monstres : 

#### Résolution des cases monstres :
* un monstre de niveau 1 => lancer un d6 :
    * si le résultat est 1 ou 2 : le héros perd 1 point de vie.
    * si le résultat est 3, 4, 5, 6 : le héros ne perd pas de point de vie, il gagne 1 pièce d'or.
* un monstre de niveau 2 => lancer un d6 :
    * si le résultat est 1, 2, 3 : le héros perd 1 point de vie.
    * si le résultat est 4, 5, 6 : le héros ne perd pas de point de vie, il gagne 3 pièces d'or.
* un monstre de niveau 3 => lancer un d6 :
    * si le résultat est 1, 2, 3 : le héros perd 2 points de vie.
    * si le résultat est 4, 5, 6 : le héros ne perd pas de point de vie, il gagne 1 pièce d'or.

### Itération 6 :
* La case '12' est une porte vérouillée, lancer un d6 :
    * Si le résultat est 1 ou 2 : la porte reste vérouillé, la partie est terminée, vous restez en vie, mais l'épée magique sera inaccessible.
    * Si le résuttat est 3, 4, 5 ou 6 la porte se dévérouille, l'aventure continue.
* Lorsque la partie est terminée, le joueur peut voir les informations suivantes : 
    * état de santé du héros (mort ou vivant),
    * le nombre de pièces d'or du héros,
    * si le héros à obtenu l'épée magique ou pas.
    
### Itération 7 : 
 * Le héros commence l'aventure avec 1 bouclier.
 * Le héros ne possède qu'un seul bouclier.
 * Un bouclier peut être dépensé lors d'un combat pour bloquer toutes les attaques d'un combat.
    * Par exemple: si le héros devait perdre 2 points de vie durant un combat, il peut bloquer cette attaque en dépensant 1 bouclier.
    * une fois utilisé, le bouclier est brisé, et donc inutilisable.
 * Sur la case '14', le héros peut s'équiper d'un nouveau bouclier s'il a déjà utilisé le sien. 
 * La case '9' contient un monstre de niveau 4 : 
    * ce monstre va automatiquement briser le bouclier du héros.
    * Si le héros n'a plus de bouclier, il ne se passe rien.

### Lexique :
 * d6 => dé à 6 faces.

